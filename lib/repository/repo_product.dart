import 'package:dio/dio.dart';
import 'package:shop_anju_app/config/config_api.dart';
import 'package:shop_anju_app/model/anju_detail_result.dart';
import 'package:shop_anju_app/model/anju_list_result.dart';

class RepoProduct {
  /**
   * 복수R
   */
  Future<AnjuListResult> getProducts() async {
    Dio dio = Dio();

    String _baseUrl = '$apiUri/product/all'; // 엔드포인트

    final response = await dio.get(_baseUrl,
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }));

    return AnjuListResult.fromJson(response.data);
  }

  Future<AnjuDetailResult> getProduct(num id) async {
    Dio dio = Dio();

    String _baseUrl = '$apiUri/product/detail/{id}';

    final response = await dio.get(_baseUrl.replaceAll('{id}', id.toString()),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }));

    return AnjuDetailResult.fromJson(response.data);
  }
}
