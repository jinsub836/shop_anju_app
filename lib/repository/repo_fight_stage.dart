import 'package:dio/dio.dart';
import 'package:shop_anju_app/config/config_api.dart';
import 'package:shop_anju_app/model/common_result.dart';
import 'package:shop_anju_app/model/fight_stage_state_result.dart';

class RepoFightStage {
  Future<FightStageStateResult> getCurrentState() async {
    Dio dio = Dio();
    String _baseUrl = '$apiUri/fight-stage/current/state'; // 엔드포인트

    final response = await dio.get(
        _baseUrl,
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            })
    );

    return FightStageStateResult.fromJson(response.data);
  }

  Future<CommonResult> setStageIn(num monsterId) async {
    Dio dio = Dio();
    String _baseUrl = '$apiUri/fight-stage/stage/in/monster-id/{monsterId}'; // 엔드포인트

    final response = await dio.post(
        _baseUrl.replaceAll('{monsterId}', monsterId.toString()),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            })
    );

    return CommonResult.fromJson(response.data);
  }

  Future<CommonResult> delStageMonster(num stageId) async {
    Dio dio = Dio();
    String _baseUrl = '$apiUri/fight-stage/stage/out/stage-id/{stageId}'; // 엔드포인트

    final response = await dio.delete(
        _baseUrl.replaceAll('{stageId}', stageId.toString()),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            })
    );

    return CommonResult.fromJson(response.data);
  }
}