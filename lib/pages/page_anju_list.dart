import 'package:flutter/material.dart';
import 'package:shop_anju_app/components/component_anju_item.dart';
import 'package:shop_anju_app/model/anju_item.dart';
import 'package:shop_anju_app/pages/page_anju_detail.dart';
import 'package:shop_anju_app/repository/repo_product.dart';

class PageAnjuList extends StatefulWidget {
  const PageAnjuList({super.key});

  @override
  State<PageAnjuList> createState() => _PageAnjuListState();
}

class _PageAnjuListState extends State<PageAnjuList> {
  List<AnjuItem> _list = [];

  Future<void> _loadList() async {
    // 이 메서드가 repo 호출해서 데이터 받아온 다음에..
    // setstate해서 _list 교체 할거다....
    await RepoProduct().getProducts()
        .then((res) => {
          print(res),
          setState(() {
            _list = res.list;
          })
        })
        .catchError((err) => {
          debugPrint(err)
        });
  }

  @override
  void initState() {
    super.initState();
    _loadList();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('안주 리스트'),
      ),
      body: ListView.builder(
        itemCount: _list.length,
        itemBuilder: (BuildContext context, int idx) {
          return ComponentAnjuItem(anjuItem: _list[idx], callback: () {
            Navigator.of(context).push(MaterialPageRoute(builder: (context) => PageAnjuDetail(id: _list[idx].id)));
          });
        },
      ),
    );
  }
}
