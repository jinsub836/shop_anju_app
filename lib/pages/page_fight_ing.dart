import 'package:flutter/material.dart';
import 'package:shop_anju_app/components/component_monster_item.dart';
import 'package:shop_anju_app/model/monster_item.dart';

class PageFightIng extends StatefulWidget {
  const PageFightIng({
    super.key,
    required this.monster1,
    required this.monster2
  });

  // 생성자를 통해서 몬스터1이랑 몬스터2 정보 받아옴.
  final MonsterItem monster1;
  final MonsterItem monster2;

  @override
  State<PageFightIng> createState() => _PageFightIngState();
}

class _PageFightIngState extends State<PageFightIng> {

  bool _isTurnMonster1 = true; // 기본으로 1번 몬스터 선빵, 1번 몬스터 순서면 2번 몬스터 순서 아님.

  bool _monster1Live = true;
  num _monster1CurrentHp = 0;

  bool _monster2Live = true;
  num _monster2CurrentHp = 0;

  String _gameLog = '';

  @override
  void initState() {
    super.initState();
    _calculateFirstTurn(); // 몬스터 선빵 판별
    setState(() {
      _monster1CurrentHp = widget.monster1.monsterHp; // 1번 몬스터 잔여 hp (페이지 들어어자마자니까 최대최력만큼)
      _monster2CurrentHp = widget.monster2.monsterHp; // 2번 몬스터 잔여 hp (페이지 들어어자마자니까 최대최력만큼)
    });
  }

  // 선빵누구인지 계산
  void _calculateFirstTurn() {
    if (widget.monster1.monsterSpeed < widget.monster2.monsterSpeed) { // 몬스터1번보다 몬스터2번의 스피드가 높으면
      setState(() {
        _isTurnMonster1 = false; // 몬스터 2번 선빵
      });
    }
  }

  // 때렸을때 최종 공격력 몇인지 (크리티컬 계산식 넣기)
  num _calculateResultHitPoint(num myHitPower, num targetDefPower) {
    List<num> criticalArr = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]; // 10% 확률로 크리티컬걸리게 할거다.
    criticalArr.shuffle(); // 섞는다.
    print(criticalArr);

    bool isCritical = false; // 기본으로 크리티컬 안터진다.
    if (criticalArr[0] == 1) isCritical = true; // 다 섞은 criticalArr에 0번째 요소가 1이면 크리뜬걸로 한다.

    num resultHit = myHitPower; // 공격력
    if (isCritical) resultHit = resultHit * 2; // 크리티컬이면 공격력 두배 뻥튀기
    resultHit = resultHit - targetDefPower; // 상대방 방어력만큼 데미지 까주기
    resultHit = resultHit.round(); // 반올림 처리

    print(resultHit);

    if(resultHit <= 0) resultHit = 1; // 계산결과 공격이 0이거나 음수일경우 공격포인트 고정1 처리
    print(resultHit);

    return resultHit;
  }

  // 상대 몬스터가 죽었는지 확인하기
  void _checkIsDead(num targetMonster) {
    if (targetMonster == 1 && (_monster1CurrentHp <= 0)) {
      setState(() {
        _monster1Live = false;
      });
    } else if (targetMonster == 2 && (_monster2CurrentHp <= 0)) {
      setState(() {
        _monster2Live = false;
      });
    }
  }

  // 공격 처리
  void _attMonster(num actionMonster) {
    num myHisPower = widget.monster1.monsterHitPower;
    num targetDefPower = widget.monster2.monsterDefensive;

    if (actionMonster == 2) {
      myHisPower = widget.monster2.monsterHitPower;
      targetDefPower = widget.monster1.monsterDefensive;
    }

    num resultHit = _calculateResultHitPoint(myHisPower, targetDefPower); // 상대 Hp 몇 까야하는지 계산

    if (actionMonster == 1) { // 공격하는 몬스터가 1번이면 (공격당하는 몬스터가 2번)
      setState(() {
        _monster2CurrentHp -= resultHit; // 2번 몬스터 체력 깎기
        if (_monster2CurrentHp <= 0) _monster2CurrentHp = 0; // 체력 0 미만이면 0으로 고정
        _checkIsDead(2); // 2번 몬스터 죽었는지 확인
      });
    } else {
      setState(() {
        _monster1CurrentHp -= resultHit; // 1번 몬스터 체력 깎기
        if (_monster1CurrentHp <= 0) _monster1CurrentHp = 0; // 체력 0 미만이면 0으로 고정
        _checkIsDead(1); // 1번 몬스터 죽었는지 확인
      });
    }

    setState(() {
      _isTurnMonster1 = !_isTurnMonster1; // 턴 넘기기. not 연산자로 계속 바꿔주기
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('배트루'),
      ),
      body: _buildBody(context),
    );
  }

  Widget _buildBody(BuildContext context) {

    return SingleChildScrollView(
      child: Column(
        children: [
          Row(
            children: [
              ComponentMonsterItem(
                monsterItem: widget.monster1,
                callback: () {
                  print('aaa');
                  _attMonster(1);
                },
                isMyTurn: _isTurnMonster1,
                isLive: _monster1Live,
                currentHp: _monster1CurrentHp,
              ),
              ComponentMonsterItem(
                monsterItem: widget.monster2,
                callback: () {
                  _attMonster(2);
                },
                isMyTurn: !_isTurnMonster1,
                isLive: _monster2Live,
                currentHp: _monster2CurrentHp,
              ),
            ],
          ),
          Text(_gameLog),
        ],
      ),
    );
  }
}
