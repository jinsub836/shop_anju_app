import 'package:flutter/material.dart';
import 'package:shop_anju_app/model/anju_response.dart';
import 'package:shop_anju_app/repository/repo_product.dart';

class PageAnjuDetail extends StatefulWidget {
  const PageAnjuDetail({
    super.key,
    required this.id
  });

  final num id;

  @override
  State<PageAnjuDetail> createState() => _PageAnjuDetailState();
}

class _PageAnjuDetailState extends State<PageAnjuDetail> {
  AnjuResponse? _detail;

  Future<void> _loadDetail() async {
    await RepoProduct().getProduct(widget.id)
        .then((res) => {
          setState(() {
            _detail = res.data;
          })
    });
  }

  @override
  void initState() {
    super.initState();
    _loadDetail();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('상세보기'),
      ),
      body: _buildBody(context),
    );
  }

  Widget _buildBody(BuildContext context) {
    if (_detail == null) { // 스켈레톤 ui
      return Text('데이터 로딩중..');
    } else {
      return ListView(
        children: [
          Text('${widget.id}'),
          Text(_detail!.thumbUrl),
          Text(_detail!.productName),
          Text('${_detail!.productPrice}'),
          Text(_detail!.dateCreate),
        ],
      );
    }
  }
}
