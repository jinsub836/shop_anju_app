import 'package:flutter/material.dart';
import 'package:shop_anju_app/model/monster_item.dart';
import 'package:shop_anju_app/pages/page_fight_ing.dart';
import 'package:shop_anju_app/repository/repo_fight_stage.dart';

class PageFightStage extends StatefulWidget {
  const PageFightStage({super.key});

  @override
  State<PageFightStage> createState() => _PageFightStageState();
}

class _PageFightStageState extends State<PageFightStage> {
  MonsterItem? monster1;
  MonsterItem? monster2;

  // 현재 결투장에 진입중인 몬스터들 정보 가져오기
  Future<void> _loadDetail() async {
    await RepoFightStage().getCurrentState()
      .then((res) => {
        setState(() {
          monster1 = res.data.monster1;
          monster2 = res.data.monster2;
        })
      });
  }

  // 결투장에서 몬스터 퇴출시키기
  Future<void> _delMonster(num stageId) async {
    await RepoFightStage().delStageMonster(stageId)
        .then((res) => {
          _loadDetail() // 퇴출에 성공하면 현재 결투장에 진입중인 몬스터 정보 새로 가져와서 다시 갱신
        });
  }

  // 페이지 생성되고 화면에 부착하기전에 데이터부터 쥐어줘야함.
  @override
  void initState() {
    super.initState();
    _loadDetail();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('전투대기실'),
      ),
      body: _buildBody(context),
    );
  }

  Widget _buildBody(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          // 디자인 영역 안에 삼항연산자로 처리하는법 중점적으로 보기.
          monster1 != null ?
          Container(
            child: Column(
              children: [
                Text(monster1!.monsterName),
                OutlinedButton(
                  onPressed: () {
                    _delMonster(monster1!.monsterStageId);
                  },
                  child: Text('퇴장'),
                )
              ],
            ),
          ) : Container(),

          monster2 != null ?
          Container(
            child: Column(
              children: [
                Text(monster2!.monsterName),
                OutlinedButton(
                  onPressed: () {
                    _delMonster(monster2!.monsterStageId);
                  },
                  child: Text('퇴장'),
                )
              ],
            ),
          ) : Container(),

          // 몬스터1이랑 몬스터2가 다 있으면 배틀 버튼 보여준다. 하나라도 없으면 빈컨테이너 보여줌.
          monster1 != null && monster2 != null ?
          OutlinedButton(
            onPressed: () {
              Navigator.of(context).push(MaterialPageRoute(builder: (context) => PageFightIng(monster1: monster1!, monster2: monster2!)));
            },
            child: Text('배틀'),
          ) : Container(),
        ],
      ),
    );
  }
}
