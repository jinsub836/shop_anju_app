import 'package:flutter/material.dart';
import 'package:shop_anju_app/model/anju_item.dart';

class ComponentAnjuItem extends StatelessWidget {
  const ComponentAnjuItem({
    super.key,
    required this.anjuItem,
    required this.callback
  });

  final AnjuItem anjuItem;
  final VoidCallback callback;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: callback,
      child: Container(
        child: Column(
          children: [
            Text(anjuItem.thumbUrl),
            Text(anjuItem.productName),
            Text('${anjuItem.productPrice}원')
          ],
        ),
      ),
    );
  }
}
