import 'package:flutter/material.dart';
import 'package:shop_anju_app/model/monster_item.dart';

class ComponentMonsterItem extends StatefulWidget {
  const ComponentMonsterItem({
    super.key,
    required this.monsterItem,
    required this.callback,
    required this.isMyTurn,
    required this.isLive,
    required this.currentHp
  });

  final MonsterItem monsterItem;
  final VoidCallback callback;
  final bool isMyTurn;
  final bool isLive;
  final num currentHp;

  @override
  State<ComponentMonsterItem> createState() => _ComponentMonsterItemState();
}

class _ComponentMonsterItemState extends State<ComponentMonsterItem> {
  double _calculateHpPercent() {
    return widget.currentHp / widget.monsterItem.monsterHp;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          SizedBox(
            width: MediaQuery.of(context).size.width / 3,
            height: MediaQuery.of(context).size.width / 3,
            child: Image.asset(
              '${widget.isLive ? widget.monsterItem.photoUrl : 'assets/dog.jpeg'}',
              fit: BoxFit.fill,
            ),
          ),
          Text(widget.monsterItem.monsterName),
          Text('총HP ${widget.monsterItem.monsterHp}'),
          Text('스피드 ${widget.monsterItem.monsterSpeed}'),
          Text('공격력 ${widget.monsterItem.monsterHitPower}'),
          Text('방어력 ${widget.monsterItem.monsterDefensive}'),
          SizedBox(
            width: MediaQuery.of(context).size.width / 2,
            child: LinearProgressIndicator(
              value: _calculateHpPercent(),
            ),
          ),
          (widget.isMyTurn && widget.isLive) ?
          OutlinedButton(
            onPressed: widget.callback,
            child: Text('공격'),
          ) : Container(),
        ],
      ),
    );
  }
}
