import 'package:shop_anju_app/model/anju_item.dart';

class AnjuListResult {
  String msg;
  num code;
  List<AnjuItem> list;
  num totalCount;

  AnjuListResult(this.msg, this.code, this.list, this.totalCount);

  factory AnjuListResult.fromJson(Map<String, dynamic> json) {
    return AnjuListResult(
      json['msg'],
      json['code'],
      json['list'] != null ? (json['list'] as List).map((e) => AnjuItem.fromJson(e)).toList() : [],
      json['totalCount']
    );
    // json으로 [{name:'ghd'}, {name:'rla'}]이런 string을 받아서 Object의 List로 바꾸고 난 후에
    // 그럼 어쨋든 List니까 한 뭉텡이씩 던져줄 수 있다.
    // 근데.. 한뭉텡이씩 던지겠다 하면서 한 뭉텡이 부르는거에 이름이 바로 e
    // 다 작은그릇으로 바꿔치기 한다음에
    // 다시 그것들을 싹 가져와서 리스트로 촥 하고 정리해줌.
  }
}