import 'package:shop_anju_app/model/anju_response.dart';

class AnjuDetailResult {
  String msg;
  num code;
  AnjuResponse data;

  AnjuDetailResult(this.msg, this.code, this.data);

  factory AnjuDetailResult.fromJson(Map<String, dynamic> json) {
    return AnjuDetailResult(
        json['msg'],
        json['code'],
        AnjuResponse.fromJson(json['data'])
    );
  }
}