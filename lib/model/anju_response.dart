class AnjuResponse {
  num id;
  String productName;
  String thumbUrl;
  num productPrice;
  String dateCreate;

  AnjuResponse(this.id, this.productName, this.thumbUrl, this.productPrice, this.dateCreate);

  factory AnjuResponse.fromJson(Map<String, dynamic> json) {
    return AnjuResponse(
        json['id'],
        json['productName'],
        json['thumbUrl'],
        json['productPrice'],
        json['dateCreate']
    );
  }
}