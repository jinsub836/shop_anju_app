class AnjuItem {
  num id;
  String productName;
  String thumbUrl;
  num productPrice;

  AnjuItem(this.id, this.productName, this.thumbUrl, this.productPrice);

  factory AnjuItem.fromJson(Map<String, dynamic> json) {
    return AnjuItem(
      json['id'],
      json['productName'],
      json['thumbUrl'],
      json['productPrice'],
    );
  }
}