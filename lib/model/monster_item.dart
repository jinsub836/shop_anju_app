class MonsterItem {
  num monsterStageId;
  num monsterId;
  String monsterName;
  String photoUrl;
  num monsterHp;
  num monsterHitPower;
  num monsterDefensive;
  num monsterSpeed;

  MonsterItem(
      this.monsterStageId,
      this.monsterId,
      this.monsterName,
      this.photoUrl,
      this.monsterHp,
      this.monsterHitPower,
      this.monsterDefensive,
      this.monsterSpeed
  );

  factory MonsterItem.fromJson(Map<String, dynamic> json) {
    return MonsterItem(
      json['monsterStageId'],
      json['monsterId'],
      json['monsterName'],
      'assets/dog.jpeg',
      json['monsterHp'],
      json['monsterHitPower'],
      json['monsterDefensive'],
      json['monsterSpeed']
    );
  }
}